﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Import_NIS_Likvidace
{
    public class SQLQueries
    {
        private static readonly string connString = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        public bool SaveToDB(List<NISLividace> NisRows)
        {
            try
            {

                using (SqlConnection connection = new SqlConnection(connString))
                {


                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = connection;
                        cmd.CommandText = "use KIS";
                        cmd.ExecuteNonQuery();
                        cmd.CommandText = "TRUNCATE TABLE KIS_Likvidace";
                        cmd.ExecuteNonQuery();
                    }
                    using (SqlCommand command = new SqlCommand("INSERT INTO KIS_LIKVIDACE ([ComputerName],[IP] ,[Descr1], [Descr2],[Likvidace])" +
                                       " VALUES (@ComputerName,@IP,@Descr1,@Descr2,@Likvidace)", connection))
                    {
                        foreach (var NisRow in NisRows)
                        {
                            command.Parameters.Clear(); 
                            command.Parameters.AddWithValue("@ComputerName", NisRow.Pocitac ?? "Wrong Entry");
                            command.Parameters.AddWithValue("@IP", NisRow.IP ?? "Wrong Entry");
                            command.Parameters.AddWithValue("@Descr1", NisRow.Pozn1 ?? "Wrong Entry");
                            command.Parameters.AddWithValue("@Descr2", NisRow.Pozn2 ?? "Wrong Entry");
                            command.Parameters.AddWithValue("@Likvidace", NisRow.Likvidace);
                            command.ExecuteNonQuery();
                        }
                    }
                }
                return true;





            }
            catch (Exception ex)
            {

                return false;
            }
        }
    }
}

