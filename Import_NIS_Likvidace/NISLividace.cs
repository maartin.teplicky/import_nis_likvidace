﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Import_NIS_Likvidace
{
    public class NISLividace
    {
        public string Pocitac { get; set; }
        public string IP { get; set; }
        public string Pozn1 { get; set; }
        public string Pozn2 { get; set; }
        public bool Likvidace { get; set; }
    }
}

